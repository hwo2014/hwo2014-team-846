from racetrack import *
from collections import defaultdict
import random
import math

class Reinforcement:

	def __init__(self, parameters):
		self.positions = list()
		self.actions = map(lambda x: x/100.0, range(0,100))
		self.rewards = list()
		self.actionHistory = list()
		self.raceTrack = None
		self.crashed = False
		self.throttle = 0
		self.speeds = list()
		self.angles = list()
		self.throttles = list()
		self.turbo = False
		self.delta_speeds = list()
		self.delta_angles = list()
		self.turboFactor = 0
		#angleprediction, speedfactor,anglefactor
		self.parameters = parameters
		# This is used for adapting to different fractions and powers, on crash
		# this value is decreased 5%
		self.error = 1

	def longlist(self):
		return [0]*9
		
	def turbo_started(self, factor):
		self.turbo = 1
		self.turboFactor = factor
		
	def turbo_ended(self):
		self.turbo = 0

	def setRacetrack(self, raceTrack):
		self.raceTrack = raceTrack
		
	def addPosition(self, positions, gameTick):
		for k in positions:
			if k['id']['name'] == "rush":
				self.myposition = k
		self.angles.append(self.myposition['angle'])
		self.positions.append((gameTick, self.myposition))
		self.get_current_speed()
		
	def crash(self, data, gameTick):
		if data["name"] == "rush":
			self.crashed = True
			self.rewards.append((gameTick,-100))
			self.error *= 0.95
			
	def getDistance(self, hist):
		return self.positions[-hist][1]["piecePosition"]['inPieceDistance']
		
	def getEndLane(self, hist):
		return self.positions[-hist][1]["piecePosition"]['lane']['endLaneIndex']
		
	def get_current_speed(self):
		if len(self.positions) > 1:
			speed = self.getDistance(1)-self.getDistance(2)
		else:
			return 0
		if speed < 0:
			speed = self.getDistance(1)+self.raceTrack.pieces[self.positions[-2][1]["piecePosition"]["pieceIndex"]].length-self.getDistance(2)
			if speed < 0 and len(self.speeds) > 0:
				speed = self.speeds[-1]
		self.speeds.append(speed)

	def getPiece(self, index):
		return self.raceTrack.pieces[index]

	def getSwitch(self, gameTick):
		if self.getCurrentPiece().switch:
			x = self.myposition['piecePosition']['pieceIndex']+1
			r = random.random()
			for i in range(x,x+len(self.raceTrack.pieces)):
				i = i%len(self.raceTrack.pieces)
				if self.raceTrack.pieces[i].angle > 30:
					return "Left"
				elif self.raceTrack.pieces[i].angle < 30:
					return "Right"
			
	def spawn(self, data, gameTick):
		if data["name"] == "rush":
			self.crashed = False
		
	def getCurrentPiece(self):
		if len(self.actionHistory) > 0:
			self.rewards.append(self.actionHistory[-1])
		return self.raceTrack.pieces[self.myposition['piecePosition']['pieceIndex']]
		
	def distanceFromNextPiece(self):
		thisPiece = self.getPiece(self.myposition['piecePosition']['pieceIndex'])
		piecePosition = self.myposition['piecePosition']["inPieceDistance"]
		return thisPiece.length-piecePosition
		
	def getnextPiece(self,it=1):
		try:
			nextPiece = self.raceTrack.pieces[self.myposition['piecePosition']['pieceIndex']+it]
		except:
			nextPiece = self.raceTrack.pieces[0]
		return nextPiece
		
	def willTurbo(self):
		nextPiece = self.getnextPiece()
		if abs(nextPiece.angle) < 30:
			return True
		else:
			return False
		
	def getThrottle(self, gameTick):
		# If the game has not started yet
		# or we have crashed, then we just floor the pedal
		if self.raceTrack is None or self.crashed == True:
			return 1
		else:
			lag = 4
			# Number of features = lag*3 + 6
			# At this point 18
			# Plus squared features: 18*2 = 36
			if len(self.speeds) > 1:
				delta_speed = abs(self.speeds[-1]-self.speeds[-2])
				delta_angle = abs(self.angles[-1]-self.angles[-2])
				self.delta_speeds.append(delta_speed)
				self.delta_angles.append(delta_angle)
			if len(self.speeds) > lag+1:
				dist = min(0,self.distanceFromNextPiece())
				variables = self.delta_speeds[-lag:] + self.delta_angles[-lag:] + self.speeds[-lag:] + [self.getnextPiece().angle, abs(self.getnextPiece().radius), self.turboFactor*self.turbo, dist, self.getnextPiece(2).angle,self.getEndLane(1)]
				squares = map(lambda x: x*x,variables)
				variables += squares
				throttle = 0
				for i in range(len(variables)):
					throttle += variables[i]*self.parameters[i]
				throttle = throttle/float(sum(variables))
				throttle = throttle*self.error
				return min(1,abs(throttle))
			else:
				self.throttles.append(1)
				return 1