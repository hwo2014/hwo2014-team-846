from racetrack import *
from collections import defaultdict
import random

from pybrain.rl.environments import Environment

class Statistical:

	def setRacetrack(self, raceTrack):
		self.raceTrack = raceTrack
		self.positions = []
		self.crashed = False
		self.throttle = 0
		
	def addPosition(self, positions, gameTick):
		for k in positions:
			if k['id']['name'] == "rush":
				self.myposition = k
		self.positions.append((gameTick, positions))
		
	def crash(self, data, gameTick):
		if data["name"] == "rush":
			self.crashed = True
			
	def spawn(self, data, gameTick):
		if data["name"] == "rush":
			self.crashed = False
		
	def getCurrentPiece(self):
		return self.raceTrack.pieces[self.myposition['piecePosition']['pieceIndex']]

	def getPiece(self, index):
		return self.raceTrack.pieces[index]
		
	def getNextPiece(self, step=1):
		try:
			return self.raceTrack.pieces[self.myposition['piecePosition']['pieceIndex']+step]
		except:
			return self.raceTrack.pieces[step]
			
	def getSwitch(self, gameTick):
		if self.getCurrentPiece().switch:
			if random.random() >= 0.5:
				return "Right"
			else:
				return "Left"
		else:
			return None
		
	def getThrottle(self, gameTick):
		if self.crashed == False:
			r = 100 - random.betavariate(1, 3)*100
			t = r/100
			self.throttle = t
		else:
			t = 0
		return t
		