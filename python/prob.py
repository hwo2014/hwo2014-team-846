import math
from collections import defaultdict
from pylab import *
from operator import itemgetter
from mpl_toolkits.mplot3d import Axes3D

def singleProbability(data, mkey):
	p = 0
	t = 0
	for i in range(len(data)):
		if data[i][mkey] == 1.0:
			p += 1.0
		t += 1.0
	return math.log(p/t)
	
def speed(data,i, m):
	return data[i][m["inPieceDistance"]]-data[i-1][m["inPieceDistance"]]
	
f = open("raceLogs.rl").read().split("\n")
i = 0
data = list()
m = dict()
for line in f:
	d = line.split("\t")
	if i > 0 and float(d[1]) > 0:
		d = [d[0]] + map(lambda x: float(x), d[1:])
		data.append(d)
	elif i == 0:
		for i in range(len(d)):
			m[d[i]] = i
	i += 1
	
angle = 45.0
radius = 100.0
lane = 0.0
t = list()
s = list()

for i in range(len(data)):
	if data[i][m["pieceAngle"]] == 45 and  data[i][m["pieceRadius"]] == 100 and data[i][m["laneEnd"]] == 0.0:
		sp = speed(data,i, m)
		angle = data[i][m["angle"]]
		pd = data[i][m["inPieceDistance"]]
		pieceIndex = data[i][m["pieceIndex"]]
		if sp > 0 and angle > 0:
			t.append((sp,angle,pd,pieceIndex))
a = list()
b = list()
c = list()
for s in t:
	if s[-1] == 4:
		a.append(s[0])
		b.append(s[1])
		c.append(s[2])

scatter(a,c)
	
#fig = plt.figure()
#ax = Axes3D(fig)
#ax.plot(a,b,c)
show()


	