import json
import os

class Piece:

	def __init__(self, id, length, angle, radius, switch):
		self.id = id
		self.length = length
		self.angle = angle
		self.radius = radius
		self.switch = switch

class RaceTrack:

	def __init__(self, data):
		self.pieces = []
		i = 0
		self.name = data["race"]["track"]["id"]
		trackfile = "racetracks/" + self.name + ".rt"
		for r in data["race"]["track"]["pieces"]:
			if "angle" not in r:
				r["angle"] = 0
			if "radius" not in r:
				r["radius"] = 0
			if "switch" not in r:
				r["switch"] = False
			if "length" not in r:
				r["length"] = abs(r["angle"]*r["radius"])/180.0
			p = Piece(i, r["length"], r["angle"], r["radius"], r["switch"])
			self.pieces.append(p)
			i += 1
		sep = "\t"
		if not os.path.exists(trackfile):
			f = open(trackfile, "w")
			for p in self.pieces:
				f.write(sep.join(map(lambda x: str(x), [p.id, p.length, p.angle, p.radius, p.switch])) + "\n") 
			f.close()
			