import json
import socket
import sys
from racetrack import *
from reinf_alg import *
#from stat_alg import *
import time
from operator import itemgetter
from multiprocessing import Pool
import cPickle

class NoobBot(object):

	def __init__(self, socket, name, key,parameters=None):
		self.socket = socket
		self.name = name
		self.key = key
		self.race_id = -1
		self.trackname = ""
		self.curLap = 0
		if parameters == None:
			self.parameters = [random.random(),random.random(),random.random(),random.random()]
		else:
			self.parameters = parameters
		self.alg = Reinforcement(self.parameters)
		self.crashLogged = False
		self.lapTime = None
		self.totalTime = 0
		if not os.path.exists("raceLogs.rl"):
			self.logFile = open("raceLogs.rl", "w")
			header = "\t".join(["track", "raceId", "pieceIndex", "laneStart", "laneEnd", "lap", "inPieceDistance", "angle", "pieceLength", "pieceAngle", "pieceRadius", "pieceSwitchable", "crashed", "throttle"])
			self.logFile.write(header)
		else:
			self.logFile = open("raceLogs.rl", "a")

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send(msg + "\n")

	def join(self):
		return self.msg("join", {"name": self.name,
								 "key": self.key})

	def log(self, msg_type, data):
		if msg_type == "carPositions":
			for d in data:
				if d["id"]["name"] == "rush" and (self.alg.crashed == False or (self.alg.crashed == True and self.crashLogged == False) ):
					index = d["piecePosition"]["pieceIndex"]
					laneStart = d["piecePosition"]["lane"]["startLaneIndex"]
					laneEnd = d["piecePosition"]["lane"]["endLaneIndex"]
					lap = d["piecePosition"]["lap"]
					inPiece = d["piecePosition"]["inPieceDistance"]
					angle = d["angle"]
					piece = self.alg.getPiece(index)
					row = "\n" + "\t".join(map(lambda x: str(x),[self.trackname, self.race_id,index, laneStart, laneEnd, lap, inPiece, angle, piece.length, piece.angle, piece.radius, piece.switch, self.alg.crashed, self.alg.throttle]))
					if self.alg.crashed == True and self.crashLogged == False:
						self.crashLogged = True
					self.logFile.write(row)
		elif msg_type == "gameStart":
			self.race_id = str(time.time())
			

	def throttle(self, throttle):
		self.msg("throttle", throttle)
		
	def switchLane(self, switch):
		self.msg("switchLane", switch)

	def ping(self):
		self.msg("ping", {})

	def run(self):
		self.join()
		return self.msg_loop()

	def on_join(self, data, gameTick):
		print("Joined")
		self.ping()

	def on_game_start(self, data, gameTick):
		print("Race started")
		self.ping()

	def on_car_positions(self, data, gameTick):
		self.alg.addPosition(data, gameTick)
		throt = self.alg.getThrottle(gameTick)
		switch = self.alg.getSwitch(gameTick)
		if switch != None:
			self.switchLane(switch)
		self.throttle(throt)

	def on_crash(self, data, gameTick):
		print("Crashed")
		self.alg.crash(data, gameTick)
		
	def on_spawn(self, data, gameTick):
		print("Spawned")
		self.crashLogged = False
		self.alg.spawn(data, gameTick)

	def on_game_end(self, data, gameTick):
		print("Race ended")
		print data
		self.ping()

	def on_error(self, data, gameTick):
		print("Error: {0}".format(data))
		self.ping()
		
	def get_race(self, data, gameTick):
		self.raceTrack = RaceTrack(data)
		self.trackname = self.raceTrack.name
		self.alg.setRacetrack(self.raceTrack)
		
	def get_turbo(self, data, gameTick):
		print "Turbo?"
		if self.alg.willTurbo():
			print "Turbo!"
			self.factor = data["turboFactor"]
			self.msg("turbo", "turbo")
			
	def turbo_start(self, data, gameTick):
		self.alg.turbo_started(self.factor)
		
	def turbo_end(self, data, gameTick):
		self.alg.turbo_ended()
		
	def lap_finished(self, data, gameTick):
		print "LapFinished", data["lapTime"]["millis"]
		self.lapTime = data["lapTime"]["millis"]
		self.curLap += 1
		self.totalTime += self.lapTime

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'spawn': self.on_spawn,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
			'gameInit': self.get_race,
			'turboAvailable': self.get_turbo,
			'turboStart': self.turbo_start,
			'turboEnd': self.turbo_end,
			'lapFinished': self.lap_finished
		}
		socket_file = self.socket.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			gameTick = -1
			if "gameTick" in msg:
				gameTick = msg['gameTick']
			if msg_type in msg_map:
				msg_map[msg_type](data, gameTick)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			self.log(msg_type, data)
			self.throttle(1)
			line = socket_file.readline()
			if optimize == True:
				if self.alg.crashed == True:
					self.lapTime = 1200000
					self.totalTime = 1200000
					break
				if self.curLap > 0:
					break
		return self.lapTime

def crossover(a,b):
	cut1 = cut2 = 0
	while cut1 != cut2:
		cut1 = random.randint(0,len(a)-1)
		cut2 = random.randint(0,len(a)-1)
	cuts = sorted((cut1,cut2))
	c = a[:cut1] + b[cut1:cut2] + a[cut2:]
	d = b[:cut1] + a[cut1:cut2] + b[cut2:]
	e = list()
	for i in range(len(a)):
		e.append((a[i]+b[i])/2.0)
	return c,d,e

def mutate(v):
	r = random.randint(0,len(v)-1)
	v[r] = random.uniform(-1,1)
	return v	

def elitist_crossover(sorted_list):
	num = len(sorted_list)-1
	paired_pairs = set()
	result = list()
	result.append(sorted_list[0][1])
	while len(result) < len(sorted_list):
		r1 = int(random.betavariate(1, 3)*(len(sorted_list)/2))
		r2 = int(random.betavariate(1, 3)*(len(sorted_list)/2))
		pm = tuple(sorted((r1,r2)))
		if r1 != r2 and pm not in paired_pairs:
			a,v,e = crossover(sorted_list[r1][1],sorted_list[r2][1])
			if random.random() < 0.1:
				v = mutate(v)
			if random.random() < 0.1:
				a = mutate(a)
			result.append(a)
			result.append(v)
			result.append(e)
			paired_pairs.add(pm)
	return result
	
def runFunction(param):
	host, port, name, key = sys.argv[1:5]	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, int(port)))
	print("Connecting with parameters:")
	print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
	print("Bot parameters: " + str(param))
	p = NoobBot(s, name, key,parameters=param)
	bestLaptime = p.run()
	s.close()
	return bestLaptime, p.parameters
	
lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey train")
	else:
		host, port, name, key = sys.argv[1:5]
		if len(sys.argv) == 6:		
			optimize = True
		else:
			optimize = False
		iteration = 0
		numParam = 36
		if optimize == True:
			id = int(time.time())
			f = open("mutations.t", "a")
			population = list()
			next_population = None
			results = list()
			size = 50
			batch = 10
			# Init
			import os
			if os.path.exists("generations/generations-0-0.p"):
				population = cPickle.load(open("generations/generations-0-0.p", "rb"))
			else:
				for i in range(size):
					pop = list()
					for i in range(numParam):
							pop.append(random.uniform(-1,1))
					population.append(pop)
			lastBestTime = 10000000
			## Initialize and make the first run
			while len(results) == 0 or lastBestTime-results[0][0] > 10 or iteration < 10000:
				if len(results) > 0:
					lastBestTime = results[0][0]
				#results = [runFunction(population[0])]
				if next_population is None:
					res = list()
					pops = lol(population,batch)
					ii = 0
					while len(res) < size:
						startTime = time.time()
						p = Pool(batch)
						res += p.map(runFunction, pops[ii])
						p.close()
						ii += 1
						t = time.time()
						while t-startTime < 60:
							print "Sleeping"
							time.sleep(1)
						print len(res)
					results = list(res)
					for i in range(len(results)):
						if results[i][0] is None:
							results[i] = list(results[i])
							results[i][0] = 40000
							results[i] = tuple(results[i])
				results = sorted(results,key=itemgetter(0))
				
				output = open('generations/generations-' + str(iteration) + "-" + str(id) + '.p', 'wb')
				cPickle.dump(results, output)
				output.close()
				
				print results
				f.write(str(results[0][0]) + "\t" + str(map(lambda x: str(x), results[0][1])) + "\n")
				population = elitist_crossover(results)
				print iteration
				iteration += 1
		else:
			print("Connecting with parameters:")
			print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect((host, int(port)))
			bot = NoobBot(s, name, key,parameters=[0.5668334959191841, -0.3142528193019032, 0.35958252998103113, -0.627573336357619, -0.256691194728772, -0.5874448607968981, 0.8587603758808819, 0.05671973455367585, -0.7886275501994213, 0.2774028127086161, -0.8449705534715701, 0.5845161435720103, -0.8125344668343955, -0.5264978186777456, 0.051376686399940974, 0.5227566883648644, 0.9503274158349886, 0.8268981327313742, -0.5125068009050331, 0.8568350463077652, 0.8009046463506595, -0.9225492005691849, 0.7290542871557475, 0.1687258653244632, 0.904925040461547, 0.8794479261835448])
			bot.run()
